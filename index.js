$(document).ready(function () {
    $('[data-toggle="datepicker"]').datepicker();

  $.ajax({
    url: "https://wegivmerchantapp.firebaseapp.com/exam/bi-member-day-2020-04-01.json",
    type: "post",
    crossDomain: true,
    dataType: "json",
    success: function (response) {
      var result = Object.entries(response.data);
      var dataListObj = response.data.list;
      var totalMemA = response.data.summarytier[0].total_members;
      var totalAmount = response.data.summarytier[0].total_amount;

      var totalTrans = response.data.summary.totaltransaction;
      var lifeimeValue = response.data.summary.lifetimevalue;
      var totalpoint = response.data.summary.totalpoint;
      var remainingPoint = response.data.summary.remainingpoint;

      console.log(totalMemA);

      document.getElementById("totalMemA").innerHTML = totalMemA;
      document.getElementById("totalAmountOrange").innerHTML = nFormatter(
        totalAmount,
        1
      );      

      document.getElementById("totalMemB").innerHTML = totalMemA;
      document.getElementById("totalAmountGray").innerHTML = nFormatter(
        totalAmount,
        1
      );

      document.getElementById("totalTrans").innerHTML = totalTrans;
      document.getElementById("lifeimeValue").innerHTML = lifeimeValue;
      document.getElementById("totalpoint").innerHTML = totalpoint;
      document.getElementById("remainingPoint").innerHTML = remainingPoint;
      



      dataListObj = dataListObj.filter(
        (dataListObj, index, self) =>
          index ===
          self.findIndex(
            (t) =>
              t.customername === dataListObj.customername &&
              t.customertier === dataListObj.customertier
          )
      );

      $("#tableid").DataTable({
        searching: false,
        info: false,
        data: dataListObj,
        columns: [
          { title: "Name", data: "customername" },
          { title: "ID", data: "customerphone" },
          { title: "Tier", data: "customertier" },
          { title: "LTV", data: "totalamount" },
          { title: "Total Trans.", data: "totaltransaction" },
          { title: "Total Point", data: "totalreward" },
          { title: "Remaining Point", data: "remainingpoint" },
        ],
      });
    },
    error: function (xhr, status) {
      alert("error");
    },
  });
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function nFormatter(num, digits) {
  const lookup = [
    { value: 1, symbol: "" },
    { value: 1e3, symbol: "k" },
    { value: 1e6, symbol: "M" },
    { value: 1e9, symbol: "G" },
    { value: 1e12, symbol: "T" },
    { value: 1e15, symbol: "P" },
    { value: 1e18, symbol: "E" },
  ];
  const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var item = lookup
    .slice()
    .reverse()
    .find(function (item) {
      return num >= item.value;
    });
  return item
    ? (num / item.value).toFixed(digits).replace(rx, "$1") + item.symbol
    : "0";
}



